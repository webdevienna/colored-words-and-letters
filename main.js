window.onload = function () {
    let paragraphText = document.querySelector('p').innerText;
    let paragraphColoredWords = document.querySelector('#coloredWords');
    let paragraphColoredLetters = document.querySelector('#coloredLetters');
    let wordsArray = paragraphText.split(" "); // split("space") - jeses Wort ist ein Element im Array
    let lettersArray = paragraphText.split(""); // split ("without Space") - jeder Buchstabe ist ein Element im Array

    let colorsArray = ["A", "B", "C", "D", "E", "F", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]; // HEX-Werte in einem Array aufgeteilt

    lettersArray.forEach( (item, index, array) => {
        if (array[index] == " ") {
            array[index] = "&nbsp;"; // fügt ein Leerzeichen in die leeren Indexes im Array ein, da sonst in der Ausgbe keine Abstände mehr zwischen den Wörtern wäre
        } else {
            array[index] = `<span style="color: ${shuffleColor(colorsArray)}"> ${item} </span>`;
        }
    });

    wordsArray.forEach( (item, index, array) => {
        array[index] = `<span style="color: ${shuffleColor(colorsArray)}"> ${item} </span>`;
    });

    paragraphColoredWords.innerHTML = wordsArray.join(""); // Ausgabe der färbigen Wörter
    paragraphColoredLetters.innerHTML = lettersArray.join(""); // Ausgabe der färbigen Buchstaben

    // Funktion - zufälliges generieren der Farben, ausgabe als HEX-Wert
    function shuffleColor (array) {
        array.sort( () => Math.random() - 0.5);
        let colorNumbers = array.slice(0,6);
        let colorCode = "#" + colorNumbers.join("");
        return colorCode;
    }
}